/*
 * @Author: YinXuan
 * @Date: 2023-02-20 09:36:03
 * @LastEditTime: 2023-02-22 16:16:25
 * @Description:
 */
import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const routes = [
	{
		path: '/',
		name: 'index',
		component: () => import(/* webpackChunkName: "Login" */ '@/pages/index'),
		children: [
			{
				path: 'originalGod',
				name: 'originalGod',
				component: () => import(/* webpackChunkName: "originalGod" */ '@/pages/demo/originalGod'),
				meta: {
					title: '模仿原神网站',
				},
			},
			{
				path: '/resume',
				name: '个人简历',
				component: () => import(/* webpackChunkName: "resume" */ '@/pages/resume/index'),
				children: [],
				meta: {
					title: '陈思顺的个人网站',
				},
			},
		],
	},
	{
		path: '/demo',
		name: 'demo',
		component: () => import(/* webpackChunkName: "resume" */ '@/pages/resume/index'),
		children: [],
	},
	{
		path: '/:by',
		component: () => import(/* webpackChunkName: "by" */ '@/pages/by/index'),
		name: 'by',
		children: [
			{
				path: 'ws',
				name: 'ws',
				component: () => import(/* webpackChunkName: "pppp" */ '@/pages/by/resume/index'),
			},
			{
				path: 'ss',
				name: 'ss',
				component: () => import(/* webpackChunkName: "pppp" */ '@/pages/by/resume1/index'),
			},
			{
				path: 'es',
				name: 'es',
				component: () => import(/* webpackChunkName: "pppp" */ '@/pages/by/resume2/index'),
			},
		],
	},
];

const router = new Router({
	mode: 'hash',
	routes,
});

// 获取原型对象push函数
const originalPush = Router.prototype.push;

// 获取原型对象replace函数
const originalReplace = Router.prototype.replace;

// 修改原型对象中的push函数
Router.prototype.push = function push(location) {
	return originalPush.call(this, location).catch((err) => err);
};

// 修改原型对象中的replace函数
Router.prototype.replace = function replace(location) {
	return originalReplace.call(this, location).catch((err) => err);
};

router.beforeEach((to, from, next) => {
	if (to.meta.title) {
		document.title = to.meta.title;
	}
	next(); //切记操作完一定要记得放行,否则无法正常跳转页面
});

export default router;
