import Vue from 'vue'
import Vuex from 'vuex'

let host = window.location.protocol + "//" + window.location.host + "/api";

Vue.use(Vuex)

export default new Vuex.Store({

    state: {
        //这里放全局参数
        userMessage: '',
        config: {
            mobile: false,
            showDrawer: false,
            debug: true
        },
        isLogin: false,
        host: host,
    },

    mutations: {

    },
})
